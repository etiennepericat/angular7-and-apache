# Set up Angular deployment on apache Web Server
Note : This is working on a **Arch linux** server. It should work on other linux distros if you adapt file paths.
## Script
You can place this deployment script for example in **/var/lib/jenkins/scripts/** :

of course **edit** *SOURCE_FILES* and *TARGET_DIR* with your needs.
~~~~
SOURCE_PROJECT="/var/lib/jenkins/workspace/YOUR-PROJECT-NAME"
SOURCE_FILES="$SOURCE_PROJECT/dist/"
TARGET_DIR="/srv/http/..." # Where you have your apache files

echo ""
echo " ## ## DEPLOYING project on the DEV environment ## ## "

echo "start building"
cd $SOURCE_PROJECT
npm install
npm run ng -- build

echo "Moving files from $SOURCE_FILES to $TARGET_DIR"

if [ -d $TARGET_DIR ]; then
	rm -r $TARGET_DIR
fi

mkdir $TARGET_DIR
mv $SOURCE_FILES $TARGET_DIR

echo "Updating permissions of $TARGET_DIR"
chown http:http -r $TARGET_DIR

echo " -- -- Deployment done -- -- "
~~~~

## Grant access
We need jenkins to run the script as root :
  - in **/etc/suoders** add
    - `jenkins    ALL = NOPASSWD: /var/lib/jenkins/scripts/your_script.sh`

## Set up jenkins
In **project configuration &rarr; Post build action** add :
`sudo /var/lib/jenkins/scripts/your_script.sh`


## Set up apache
**httpd.conf** need to be as follows to work with Angular 6/7 routing :

**important** : check the RewriteCond and RewriteUrl part
~~~~
...

Listen 80
Listen 8080
ServerRoot "/etc/httpd"
DocumentRoot "/srv/http/" # probably /var/www on Ubuntu (or something like that)
ServerAdmin your.name@gmail.com

<VirtualHost *:80>
    ServerName WHAT_YOU_WANT

    DocumentRoot /pathToYourFiles

    <Directory /pathToYourFiles>
        RewriteEngine on

        # Don't rewrite files or directories
        RewriteCond %{REQUEST_FILENAME} -f [OR]
        RewriteCond %{REQUEST_FILENAME} -d
        RewriteRule ^ - [L]

        # Rewrite everything else to index.html
        # to allow html5 state links
        RewriteRule ^ index.html [L]
    </Directory>
</VirtualHost>

...
~~~~

To see the complete file go see it on this repository.
