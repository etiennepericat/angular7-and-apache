SOURCE_PROJECT="/var/lib/jenkins/workspace/YOUR-PROJECT-NAME"
SOURCE_FILES="$SOURCE_PROJECT/dist/"
TARGET_DIR="/srv/http/..." # Where you have your apache files

echo ""
echo " ## ## DEPLOYING project on the DEV environment ## ## "

echo "start building"
cd $SOURCE_PROJECT
npm install
npm run ng -- build

echo "Moving files from $SOURCE_FILES to $TARGET_DIR"

if [ -d $TARGET_DIR ]; then
	rm -r $TARGET_DIR
fi

mkdir $TARGET_DIR
mv $SOURCE_FILES $TARGET_DIR

echo "Updating permissions of $TARGET_DIR"
chown http:http -r $TARGET_DIR

echo " -- -- Deployment done -- -- "
